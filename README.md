# Todeslang CGI

https://ai18n.tk/todeslang

## Install

Depends on:

* https://gitlab.com/hakabahitoyo/todeslang-parser
* https://gitlab.com/hakabahitoyo/todeslang-interpreter

Steps:

```
cd /var/www/html
sudo ln -s .../todeslang-cgi/client todeslang
cd .../todeslang-cgi/server
sudo ./install
```
